package com.example.javaexercise.array;

public class BubbleSort {

	public void bubbleSort(int[] array) {
		for(int i=0; i<array.length; i++) {
			for(int j=1; j<array.length-i; j++) {
				if(array[j-1] >array[j]) {
					int tmp = array[j-1];
					array[j-1] = array[j];
					array[j] = tmp;
				}
			}
		}
		
		System.out.println(array);
	}

	public static void main(String[] args) {
		int array[] = { 5, 50, 10, 40, 20, 80, 30 };
		BubbleSort bubbleSort = new BubbleSort();
		bubbleSort.bubbleSort(array);
	}

}

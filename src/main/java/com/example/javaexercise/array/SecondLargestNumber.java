package com.example.javaexercise.array;

import com.example.javaexercise.ExerciseException;

public class SecondLargestNumber {

	public int secondLargestNumber(int[] array) throws ExerciseException {

		int first = array[0];
		int second = array[0];
		int third = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > first) {
				third = second;
				second = first;
				first = array[i];
			} else if (array[i] > second) {
				second = array[i];
			} else if (array[i] > third) {
				third = array[i];
			}
		}
		return third;
	}

	public static void main(String[] args) {
		int array[] = { 5, 50, 10, 40, 20, 80, 30 };
		SecondLargestNumber secondLargestNumber = new SecondLargestNumber();

		try {
			int second = secondLargestNumber.secondLargestNumber(array);
			System.out.println("Second Largest Numer>>>>>>" + second);
		} catch (ExerciseException e) {
			e.printStackTrace();
		}

	}

}

package com.example.javaexercise.array;

public class LargestNumber {

	int array[] = { 10, 30, 50, 20, 70 };

	public int findLargestNumber() {
		int largest = array[0];
		for (int i = 0; i < array.length; i++) {
			if(array[i]>largest) {
				largest = array[i];
			}
		}
		return largest;
	}

	public static void main(String[] args) {

		LargestNumber largestNumber = new LargestNumber();
		int largest = largestNumber.findLargestNumber();
		System.out.println("Largest Number>>>>>>>>>>>"+largest);
	}

}

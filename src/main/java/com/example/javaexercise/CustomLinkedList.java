package com.example.javaexercise;

class Node {
	int data;
	Node next;

	public void displayNodeData() {
		System.out.println("{ " + data + " } ");
	}
}

public class CustomLinkedList {

	Node head = null;

	public void insertFirst(int data) {
		Node newNode = new Node();
		if (head == null) {
			newNode.data = data;
			newNode.next = null;
		} else {
			newNode.data = data;
			newNode.next = head;
		}
		head = newNode;
	}

	public void insertLast(int data) {
		if (head == null) {
			System.out.println("List is Empty");
		}
		Node current = head;
		while (current.next != null) {
			current = current.next;
		}
		Node newNode = new Node();
		newNode.data = data;
		newNode.next = null;
		current.next = newNode;
	}

	public void deleteFirst() {
		head = head.next;
	}

	public void deleteLast() {
		Node prevos = head;
		Node current = null;
		while (prevos.next != null) {
			current = prevos;
			prevos = prevos.next;
		}
		current.next = null;
	}

	public void deleteAfter(Node after) {

		Node temp = head;
		while (temp.next != null && temp.data != after.data) {
			temp = temp.next;
		}
	}

	public boolean loopExistInList() {
		Node slowPointer, fastPointer; 
		slowPointer = fastPointer = head; 
		
		while(fastPointer!=null && fastPointer.next !=null) {
			fastPointer = fastPointer.next.next;
			slowPointer = slowPointer.next;
		}
		
		if(fastPointer == slowPointer) {
			return true;
		}
		
		
		return false;
	}
	
	public Node findMiddleNode()
	{
		Node slowPointer, fastPointer; 
		slowPointer = fastPointer = head; 
 
		while(fastPointer !=null) { 
			fastPointer = fastPointer.next; 
			if(fastPointer != null && fastPointer.next != null) { 
				slowPointer = slowPointer.next; 
				fastPointer = fastPointer.next; 
			} 
		} 
		
		System.out.println("Latest>>>" +slowPointer);
 
		return slowPointer; 
 
	}

	public static void main(String[] args) {

		CustomLinkedList customLinkedList = new CustomLinkedList();
		customLinkedList.insertFirst(10);
		customLinkedList.insertFirst(20);

		customLinkedList.insertLast(40);

		customLinkedList.insertFirst(30);

		Node current = customLinkedList.head;
		while (current != null) {
			current.displayNodeData();
			current = current.next;
		}
		
		//customLinkedList.findMiddleElement();
		customLinkedList.findMiddleNode();

		//customLinkedList.deleteFirst();
		//customLinkedList.deleteLast();
		
	}

}

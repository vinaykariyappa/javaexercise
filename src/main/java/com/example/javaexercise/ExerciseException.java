package com.example.javaexercise;

public class ExerciseException extends Exception {

	public ExerciseException(String message) {
		super(message);
	}
}
